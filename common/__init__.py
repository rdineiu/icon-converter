from os.path import realpath, dirname, join as path_join
from sys import argv

BASE_PATH = realpath(dirname(path_join(argv[0], '..', '..', '..', '')))
SOURCE_PATH = path_join(BASE_PATH, 'Application')
ASSETS_PATH = path_join(BASE_PATH, 'Assets')
