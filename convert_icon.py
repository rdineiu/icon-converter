# coding=utf-8

import sys

from converter.icon_converter import convert

if __name__ == '__main__':
    if len(sys.argv) != 2:
        sys.exit(1)

    convert(sys.argv[1])
