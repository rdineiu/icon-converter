import re

from string import Template
from getpass import getuser
from time import strftime

_top_comment_template = """\
//
//  $file_name
//  Application
//
//  Created by $user on $time.
//  Copyright (c) 2014 Company. All rights reserved.
//
//  Generated file. Do not edit.
//\
"""

_header_file_template = """\
$top_comment

$framework_imports

$functions
"""

_implementation_file_template = """\
$top_comment

$framework_imports

#import "$header_file_name"

$macros

$functions
"""

_NEWLINES_RE = re.compile(r'\n{3,}')


def _format_file(text):
    return _NEWLINES_RE.sub('\n\n', text)


class ObjCUnit:
    _functions = []
    _macros = []
    _frameworks = []

    def __init__(self, unit_name):
        self.unit_name = unit_name

    def add_function(self, name, arguments, return_type, body):
        definition = return_type

        if not return_type.endswith('*'):
            definition += ' '
        definition += name

        if len(arguments) > 0:
            definition += '(' + ', '.join(arguments) + ')'
        else:
            definition += '(void)'

        self._functions.append({
            'definition': definition,
            'body': body
        })

    def add_macro(self, name, body):
        self._macros.append({
            'name': name,
            'body': body
        })

    def add_framework(self, name, is_public=True):
        self._frameworks.append({
            'name': name,
            'public': is_public
        })

    def get_file_name(self, is_header):
        return self.unit_name + '.' + ('h' if is_header else 'm')

    def get_top_comment(self, is_header):
        return Template(_top_comment_template).substitute({
            'file_name': self.get_file_name(is_header),
            'user': getuser().title(),
            'time': strftime('%d/%m/%y')
        })

    def get_framework_imports(self, is_public=True):
        frameworks = []
        for framework in self._frameworks:
            if framework['public'] == is_public:
                frameworks.append('@import %s;' % framework['name'])

        return '\n'.join(frameworks)

    def get_function_declarations(self):
        return '\n'.join([f['definition'] + ';' for f in self._functions])

    def get_function_implementations(self):
        implementations = []

        for function in self._functions:
            implementation = function['definition'] + ' {\n'
            for statement in function['body']:
                line = str(statement)
                if len(line) > 0:
                    line = '    ' + line
                line += '\n'
                implementation += line
            implementation += '}'
            implementations.append(implementation)

        return '\n\n'.join(implementations)

    def get_macros(self):
        return '\n'.join([
            '#define ' + m['name'] + ' ' + m['body'] for m in self._macros
        ])

    def get_header_file(self):
        return _format_file(Template(_header_file_template).substitute({
            'top_comment': self.get_top_comment(is_header=True),
            'framework_imports': self.get_framework_imports(is_public=True),
            'functions': self.get_function_declarations()
        }))

    def get_implementation_file(self):
        return _format_file(Template(_implementation_file_template).substitute({
            'top_comment': self.get_top_comment(is_header=False),
            'framework_imports': self.get_framework_imports(is_public=False),
            'header_file_name': self.get_file_name(is_header=True),
            'macros': self.get_macros(),
            'functions': self.get_function_implementations()
        }))

    def get_file(self, is_header):
        if is_header:
            return self.get_header_file()
        else:
            return self.get_implementation_file()

    def write(self, file_path, is_header):
        f = open(file_path, 'w')
        f.write(self.get_file(is_header))
        f.close()
