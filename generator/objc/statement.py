class ObjCStatement:
    def __init__(self):
        pass


class ObjCVar(ObjCStatement):
    def __init__(self, name, var_type, value):
        ObjCStatement.__init__(self)
        self.name = name
        self.type = var_type
        self.value = value

    def __repr__(self):
        code = self.type
        if not code.endswith('*'):
            code += ' '
        code += self.name + ' = ' + self.value + ';'
        return code


class ObjCCall(ObjCStatement):
    def __init__(self, function, args):
        ObjCStatement.__init__(self)
        self.function = function
        self.args = args

    def __repr__(self):
        call = self.function + '('

        if len(self.args) > 0:
            call += ', '.join(self.args)

        call += ');'

        return call


class ObjCMsgSend(ObjCStatement):
    def __init__(self, target, action, args=None):
        ObjCStatement.__init__(self)
        self.target = target
        self.action = action
        self.args = args if args else []

    def __repr__(self):
        call = self.action.replace(':', ':%s ') % tuple(self.args)
        return '[%s %s];' % (self.target, call.strip())


class ObjCReturn(ObjCStatement):
    def __init__(self, expr):
        ObjCStatement.__init__(self)
        self.expr = expr

    def __repr__(self):
        return 'return ' + self.expr + ';'
