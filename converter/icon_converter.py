from os.path import join
from svg.parser import parse
from svg.command import *
from common import *
from generator.objc.unit import ObjCUnit
from generator.objc.statement import *

_point_macro = 'PT'
_value_format = '{:.3f}'


class IconConverter:
    def __init__(self, icon_name):
        self.icon_name = icon_name

    def _calculate_values(self):
        values = []

        for command in self.commands:
            if isinstance(command, SVGCurveCommand):
                for control_point in command.control_points:
                    values += [control_point.x, control_point.y]

            if isinstance(command, SVGToCommand):
                values += [command.to.x, command.to.y]

        self.value_range = min(values), max(values)

    def _map_value_to_range(self, value):
        val_min, val_max = self.value_range
        return float(value - val_min) / float(val_max - val_min)

    def _parse_commands(self):
        path = join(ASSETS_PATH, 'Icons')
        self.commands = parse(join(path, '%s.svg' % self.icon_name))

    def _format_value(self, value):
        return _value_format.format(self._map_value_to_range(value))

    def _process_position(self, position):
        return '%s(%s, %s)' % (_point_macro,
                               self._format_value(position.x),
                               self._format_value(position.y))

    def _process_point_call(self, path_var, action, positions):
        args = []
        for position in positions:
            args.append(self._process_position(position))
        return ObjCMsgSend(path_var, action, args)

    def _process_move(self, path_var, command):
        target = 'moveToPoint:'
        args = [command.to]
        return self._process_point_call(path_var, target, args)

    def _process_line(self, path_var, command):
        target = 'addLineToPoint:'
        args = [command.to]
        return self._process_point_call(path_var, target, args)

    def _process_curve(self, path_var, command):
        target = 'addCurveToPoint:controlPoint1:controlPoint2:'
        args = [command.to] + list(command.control_points)
        return self._process_point_call(path_var, target, args)

    @staticmethod
    def _process_close_path(path_var):
        return ObjCMsgSend(path_var, 'closePath')

    @staticmethod
    def _process_padding(body, command):
        if isinstance(command, SVGMoveCommand):
            body.append('')

    def _process_command(self, path_var, body, command):
        statements = []

        if isinstance(command, SVGMoveCommand):
            statements.append(self._process_move(path_var, command))
        elif isinstance(command, SVGLineCommand):
            statements.append(self._process_line(path_var, command))
        elif isinstance(command, SVGCurveCommand):
            statements.append(self._process_curve(path_var, command))
        elif isinstance(command, SVGClosePathCommand):
            statements.append(self._process_close_path(path_var))

        self._process_padding(body, command)
        body += statements

    def _generate_body(self):
        path_var = 'path'

        body = [ObjCVar(
            path_var, 'UIBezierPath *const', '[UIBezierPath bezierPath]')]

        for command in self.commands:
            self._process_command(path_var, body, command)

        body += ['', ObjCReturn('@[%s]' % path_var)]

        return body

    def _generate_unit(self, body):
        unit_name = self.icon_name + 'Icon'
        function_name = unit_name + 'Draw'

        unit = ObjCUnit(unit_name)
        unit.add_framework('Foundation', is_public=True)
        unit.add_framework('UIKit.UIBezierPath', is_public=False)
        unit.add_macro(
            _point_macro + '(x, y)',
            'CGPointMake((CGFloat)((x) * scale), (CGFloat)((y) * scale))')
        unit.add_function(
            function_name, ['const CGFloat scale'], 'NSArray *', body)

        return unit

    def get_unit(self):
        self._parse_commands()

        if len(self.commands) == 0:
            return None

        self._calculate_values()

        return self._generate_unit(self._generate_body())


def convert(icon_name):
    unit = IconConverter(icon_name).get_unit()

    if unit is None:
        return

    for is_header in (True, False):
        unit.write(join(SOURCE_PATH, unit.get_file_name(is_header)), is_header)
