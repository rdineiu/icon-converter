class SVGCommand:
    def __init__(self, name):
        assert isinstance(name, str)
        self.name = name
        pass

    def __str__(self):
        return self.name


class SVGPosition:
    def __init__(self, x, y):
        assert isinstance(x, float)
        assert isinstance(y, float)
        self.x, self.y = x, y

    def __str__(self):
        return str(self.x) + ', ' + str(self.y)


class SVGToCommand(SVGCommand):
    def __init__(self, to, name):
        assert isinstance(to, SVGPosition)
        SVGCommand.__init__(self, name)
        self.to = to

    def __str__(self):
        return SVGCommand.__str__(self) + ' to ' + str(self.to)


class SVGMoveCommand(SVGToCommand):
    def __init__(self, to):
        SVGToCommand.__init__(self, to, 'move')


class SVGLineCommand(SVGToCommand):
    def __init__(self, to):
        SVGToCommand.__init__(self, to, 'line')


class SVGCurveCommand(SVGToCommand):
    def __init__(self, to, control_points):
        assert isinstance(control_points, tuple)
        SVGToCommand.__init__(self, to, 'curve')
        self.control_points = control_points

    def __str__(self):
        description = SVGToCommand.__str__(self)

        points = self.control_points
        control_points_str = []
        for i in xrange(0, len(points)):
            control_points_str.append('cp' + str(i + 1) + ' ' + str(points[i]))

        description += ' ' + ' '.join(control_points_str)

        return description


class SVGClosePathCommand(SVGCommand):
    def __init__(self):
        SVGCommand.__init__(self, 'close')
        pass
