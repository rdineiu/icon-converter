# http://www.w3.org/TR/SVG/paths.html
# http://zvon.org/comp/r/ref-SVG_1_1_Full.html#Attributes~d

import re
from copy import copy

from command import *
from common import SVG_FLOAT_RE

_COMMANDS = set('MmZzLlHhVvCcSsQqTt')
_COMMAND_RE = re.compile(r'([MmZzLlHhVvCcSsQqTt])')

_CMD_MOVE = 'M'
_CMD_CLOSE_PATH = 'Z'
_CMD_LINE = 'L'
_CMD_LINE_HORIZONTAL = 'H'
_CMD_LINE_VERTICAL = 'V'
_CMD_CURVE_CUBIC = 'C'
_CMD_CURVE_CUBIC_SMOOTH = 'S'
_CMD_CURVE_QUADRATIC = 'Q'
_CMD_CURVE_QUADRATIC_SMOOTH = 'T'

_XY_BY_COMMAND = {
    _CMD_MOVE: (0, 1),
    _CMD_LINE: (0, 1),
    _CMD_LINE_HORIZONTAL: (0, None),
    _CMD_LINE_VERTICAL: (None, 0),
    _CMD_CURVE_CUBIC: (4, 5),
    _CMD_CURVE_CUBIC_SMOOTH: (2, 3),
    _CMD_CURVE_QUADRATIC: (2, 3),
    _CMD_CURVE_QUADRATIC_SMOOTH: (0, 1)
}

_STRIDE_BY_COMMAND = {
    _CMD_MOVE: 2,
    _CMD_LINE: 2,
    _CMD_LINE_HORIZONTAL: 1,
    _CMD_LINE_VERTICAL: 1,
    _CMD_CURVE_CUBIC: 6,
    _CMD_CURVE_CUBIC_SMOOTH: 4,
    _CMD_CURVE_QUADRATIC: 4,
    _CMD_CURVE_QUADRATIC_SMOOTH: 2
}

_KEY_COMMAND = 'command'
_KEY_PARAMS = 'params'
_KEY_X = 'x'
_KEY_Y = 'y'


def _tokenize_path_data(path_data):
    for command in _COMMAND_RE.split(path_data):
        if command in _COMMANDS:
            yield command
        for token in SVG_FLOAT_RE.findall(command):
            yield token


def _parse_xy(command_upper, params, x, y):
    if command_upper in _XY_BY_COMMAND:
        x_pos, y_pos = _XY_BY_COMMAND[command_upper]
        return \
            float(params[x_pos] if x_pos is not None else x), \
            float(params[y_pos] if y_pos is not None else y)
    else:
        return 0.0, 0.0


def _reflect_control_point(prev_command, default_point):
    if isinstance(prev_command, SVGCurveCommand):
        prev_to = prev_command.to
        prev_cp1 = prev_command.control_points[-1]
        return SVGPosition(prev_cp1.x - 2 * (prev_cp1.x - prev_to.x),
                           prev_cp1.y - 2 * (prev_cp1.y - prev_to.y))
    else:
        return default_point


# Establish a new current point.
# Params: (x, y)
def _process_move(first_command, params):
    if isinstance(first_command, SVGMoveCommand):
        return _process_line(params)

    x_pos, y_pos = _XY_BY_COMMAND[_CMD_MOVE]
    return SVGMoveCommand(to=SVGPosition(params[x_pos], params[y_pos]))


# Ends the current subpath and causes an automatic straight line to be drawn
# from the current point to the initial point of the current subpath.
# Params: (x, y)
def _process_close_path():
    return SVGClosePathCommand()


# Draw a line from the current point to the given (x, y) coordinate which
# becomes the new current point.
# Params: (x, y)
def _process_line(params):
    x_pos, y_pos = _XY_BY_COMMAND[_CMD_LINE]
    return SVGLineCommand(to=SVGPosition(params[x_pos], params[y_pos]))


# Draws a horizontal line from the current point (cpx, cpy) to (x, cpy).
# Params: (x)
def _process_line_horizontal(params, y):
    x_pos, _ = _XY_BY_COMMAND[_CMD_LINE_HORIZONTAL]
    return SVGLineCommand(to=SVGPosition(params[x_pos], y))


# Draws a vertical line from the current point (cpx, cpy) to (cpx, y).
# Params: (y)
def _process_line_vertical(params, x):
    _, y_pos = _XY_BY_COMMAND[_CMD_LINE_VERTICAL]
    return SVGLineCommand(to=SVGPosition(x, params[y_pos]))


# Draws a cubic Bezier curve from the current point to (x, y) using (x1, y1) as
# the control point at the beginning of the curve and (x2, y2) as the control
# point at the end of the curve.
#
# Params: (x1, y1, x2, y2, x, y)
def _process_curve_cubic(params):
    x_pos, y_pos = _XY_BY_COMMAND[_CMD_CURVE_CUBIC]
    return SVGCurveCommand(to=SVGPosition(params[x_pos], params[y_pos]),
                           control_points=(SVGPosition(params[0], params[1]),
                                           SVGPosition(params[2], params[3])))


# Draws a cubic Bezier curve from the current point to (x, y). The first control
# point is assumed to be the reflection of the second control point on the
# previous command relative to the current point.
#
# Params: (x2, y2, x, y)
def _process_curve_cubic_smooth(params, prev_command):
    x_pos, y_pos = _XY_BY_COMMAND[_CMD_CURVE_CUBIC_SMOOTH]

    to = SVGPosition(params[x_pos], params[y_pos])
    cp1 = _reflect_control_point(prev_command, to)
    cp2 = SVGPosition(params[0], params[1])

    return SVGCurveCommand(to=to, control_points=(cp1, cp2))


# Draws a quadratic Bezier curve from the current point to (x, y) using (x1, y1)
# as the control point.
#
# Params: (x1, y1, x, y)
def _process_curve_quadratic(params):
    x_pos, y_pos = _XY_BY_COMMAND[_CMD_CURVE_QUADRATIC]

    to = SVGPosition(params[x_pos], params[y_pos])
    cp1 = SVGPosition(params[0], params[1])
    cp2 = copy(cp1)

    return SVGCurveCommand(to=to, control_points=(cp1, cp2))


# Draws a quadratic Bezier curve from the current point to (x, y). The control
# point is assumed to be the reflection of the control point on the previous
# command relative to the current point.
#
# Params: (x, y)
def _process_curve_quadratic_smooth(params, prev_command):
    x_pos, y_pos = _XY_BY_COMMAND[_CMD_CURVE_QUADRATIC_SMOOTH]

    to = SVGPosition(params[x_pos], params[y_pos])
    cp1 = _reflect_control_point(prev_command, to)
    cp2 = copy(cp1)

    return SVGCurveCommand(to=to, control_points=(cp1, cp2))


def _process_command(first_command, prev_command, command_upper, params, x, y):
    if command_upper == _CMD_MOVE:
        return _process_move(first_command, params)
    elif command_upper == _CMD_CLOSE_PATH:
        return _process_close_path()
    elif command_upper == _CMD_LINE:
        return _process_line(params)
    elif command_upper == _CMD_LINE_HORIZONTAL:
        return _process_line_horizontal(params, y)
    elif command_upper == _CMD_LINE_VERTICAL:
        return _process_line_vertical(params, x)
    elif command_upper == _CMD_CURVE_CUBIC:
        return _process_curve_cubic(params)
    elif command_upper == _CMD_CURVE_CUBIC_SMOOTH:
        return _process_curve_cubic_smooth(params, prev_command)
    elif command_upper == _CMD_CURVE_QUADRATIC:
        return _process_curve_quadratic(params)
    elif command_upper == _CMD_CURVE_QUADRATIC_SMOOTH:
        return _process_curve_quadratic_smooth(params, prev_command)


def _update_param_values(command_upper, params, x, y):
    if command_upper == _CMD_LINE_HORIZONTAL:
        for i in xrange(0, len(params)):
            params[i] += x
    elif command_upper == _CMD_LINE_VERTICAL:
        for i in xrange(0, len(params)):
            params[i] += y
    else:
        for i in xrange(0, len(params), 2):
            params[i] += x
            params[i + 1] += y


def _parse_command(commands, data):
    command = data[_KEY_COMMAND]
    command_upper = command.upper()
    all_params = map(float, data[_KEY_PARAMS])
    is_absolute = command.isupper()
    first_command = None

    if command_upper in _STRIDE_BY_COMMAND:
        stride = _STRIDE_BY_COMMAND[command_upper]
        param_range = xrange(0, len(all_params), stride)
    else:
        stride = 0
        param_range = [0]

    for start in param_range:
        params = all_params[start:][:stride]

        x, y = data[_KEY_X], data[_KEY_Y]
        if not is_absolute:
            _update_param_values(command_upper, params, x, y)

        prev_command = commands[-1] if len(commands) > 0 else None

        command = _process_command(
            first_command, prev_command, command_upper, params, x, y)
        assert isinstance(command, SVGCommand)

        if first_command is None:
            first_command = command

        commands.append(command)

        data[_KEY_X], data[_KEY_Y] = _parse_xy(command_upper, params, x, y)


def parse_path(path_data):
    current_data = {
        _KEY_COMMAND: None,
        _KEY_PARAMS: [],
        _KEY_X: 0.0,
        _KEY_Y: 0.0
    }

    commands = []

    for token in _tokenize_path_data(path_data):
        if token in _COMMANDS:
            if current_data[_KEY_COMMAND] is not None:
                _parse_command(commands, current_data)
                current_data[_KEY_PARAMS] = []
            current_data[_KEY_COMMAND] = token
        else:
            current_data[_KEY_PARAMS].append(token)

    if current_data[_KEY_COMMAND] is not None:
        _parse_command(commands, current_data)

    return commands
