from xml.sax import make_parser, ContentHandler
from xml.sax.handler import *

from path import *
from shape import *


class ParseHandler(ContentHandler):
    commands = []

    def startElement(self, name, attrs):
        if name == 'path' and 'd' in attrs:
            self.commands += parse_path(attrs['d'])
        elif name == 'polyline' and 'points' in attrs:
            self.commands += parse_polyline(attrs['points'])
        elif name == 'polygon' and 'points' in attrs:
            self.commands += parse_polygon(attrs['points'])


def parse(file_name):
    handler = ParseHandler()
    parser = make_parser()
    parser.setFeature(feature_validation, False)
    parser.setFeature(feature_external_ges, False)
    parser.setFeature(feature_external_pes, False)
    parser.setContentHandler(handler)
    parser.parse(file_name)
    return handler.commands
