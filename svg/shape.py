# http://www.w3.org/TR/SVG/shapes.html

from common import SVG_FLOAT_RE
from command import *


def parse_polyline(point_data):
    params = map(float, SVG_FLOAT_RE.findall(point_data))

    commands = [SVGMoveCommand(to=SVGPosition(params[0], params[1]))]
    for i in xrange(2, len(params), 2):
        commands.append(SVGLineCommand(to=SVGPosition(params[i], params[i+1])))

    return commands


def parse_polygon(point_data):
    return parse_polyline(point_data) + [SVGClosePathCommand()]
